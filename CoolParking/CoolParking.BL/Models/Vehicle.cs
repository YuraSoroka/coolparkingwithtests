﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System.Text;
using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Services;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;

        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; set; }

        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            id = Validation(Id);
            this.VehicleType = VehicleType;
            if (Balance < 0)
                throw new ArgumentException();
            else
            {
                this.Balance = Balance;
            }

        }

        public string Id             // свойство для чтения
        {
            get { return id; }
            /*set
            //{
            //    string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";    // YY-DDDD-YY pattern
            //    if (Regex.IsMatch(value, pattern))
            //        id = value;
            //    else
            //        Console.WriteLine("Неверный айди !");
            //
            //    //Regex regex = new Regex(@"\w{2}-\d{4}-\w{2}");           
            }*/
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            char[] digits = "0123456789".ToCharArray();

            StringBuilder stringBuilder = new StringBuilder();
            Random randomChar = new Random();

            string TwoLetters = string.Empty;
            string FourDigits = string.Empty;

            for (int i = 0; i < 2; i++)
            {
                int index = randomChar.Next(0, 26);
                TwoLetters += alphabet[index];
            }
            stringBuilder.Append(TwoLetters.ToUpper());
            stringBuilder.Append('-');

            Random randomNumber = new Random(); //stringBuilder.Append($"{randomNumber.Next(1000, 9999)}");
            for (int i = 0; i < 4; i++)
            {
                int index = randomNumber.Next(0, 10);
                FourDigits += digits[index];
            }

            stringBuilder.Append(FourDigits);
            stringBuilder.Append('-');
            TwoLetters = string.Empty;

            for (int i = 0; i < 2; i++)
            {
                int index = randomChar.Next(0, 26);
                TwoLetters += alphabet[index];
            }

            stringBuilder.Append(TwoLetters.ToUpper());

            return stringBuilder.ToString();
        }

        private string Validation(string Id)
        {
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
            if (Regex.IsMatch(Id, pattern) && !(new ParkingService().Duplication(Id)))
            {
                this.id = Id;
                return Id;
            }
            else
            {
                throw new ArgumentException();
                //Console.WriteLine("Неверный айди ! Номер будет сгенерирован автоматически ");
                //return GenerateRandomRegistrationPlateNumber();
            }

        }
    }
}