﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {

        private static Parking instance;
        private Parking()
        {
        }
        public static Parking getInstance()                // Singleton реалізація
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        private List<Vehicle> vehicles;
        public List<Vehicle> Vehicles
        {
            get
            {
                if (vehicles == null)
                {
                    return vehicles = new List<Vehicle>();
                }
                return vehicles;
            }
            set { }
        }
        public decimal ParkingMoneyBalance { get; set; }


        private List<TransactionInfo> transactions; // транзакции
        public List<TransactionInfo> Transactions
        {
            get
            {
                if (transactions == null)
                {
                    return transactions = new List<TransactionInfo>();
                }
                return transactions;
            }
            set { }
        }

        public Vehicle FindAutoByNumber(string carNumber)
        {
            return vehicles.Find((vehicle) => { return carNumber == vehicle.Id; });
        }

        public void GetTransaction(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (Vehicle vehicle in Vehicles)
            {
                decimal amount;
                // снимает плату за 5 секунд
                if (vehicle.Balance >= Settings._tariffs[vehicle.VehicleType])
                {
                    amount = Settings._tariffs[vehicle.VehicleType];
                }
                //else if (vehicle.Balance > 0 && vehicle.Balance < Settings._tariffs[vehicle.VehicleType])
                //{
                //    amount = ((Settings._tariffs[vehicle.VehicleType] - vehicle.Balance) * Settings.fine) + vehicle.Balance;
                //
                //}
                else
                {
                    amount = Settings.fine * Settings._tariffs[vehicle.VehicleType];
                }

                TransactionInfo transactionInfo = new TransactionInfo(DateTime.Now, vehicle.Id, amount);
                Transactions.Add(transactionInfo);
            }
        }


        public void WriteOffFunds()
        {

            foreach (Vehicle vehicle in Vehicles)
            {
                decimal writtenOffFunds;

                if (vehicle.Balance >= Settings._tariffs[vehicle.VehicleType])
                {
                    writtenOffFunds = Settings._tariffs[vehicle.VehicleType];
                }
                else if(vehicle.Balance > 0 && vehicle.Balance < Settings._tariffs[vehicle.VehicleType])
                {
                    writtenOffFunds = ((Settings._tariffs[vehicle.VehicleType] - vehicle.Balance) * Settings.fine) + vehicle.Balance;
                
                }
                else
                {
                    writtenOffFunds = Settings.fine * Settings._tariffs[vehicle.VehicleType];
                }

                vehicle.Balance -= writtenOffFunds;

                ParkingMoneyBalance += writtenOffFunds;
            }
        }

    }
}