﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        //consts
        public const int PayPeriod = 5000; // в милисекундах
        public const int LogPeriod = 60000; //
        public const decimal fine = 2.5M;

        //public static int fullcapacity { get; } = 10;

        private static int capacity = 10;        
        
        private static Dictionary<VehicleType, decimal> tariffs = new Dictionary<VehicleType, decimal>()
        {
            [VehicleType.Truck] = 5M,
            [VehicleType.PassengerCar] = 2M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Motorcycle] = 1M
        };

        public static decimal _startBalance { get; set; } // автосвойство
        public static int _capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value;
            }
        }
        public static Dictionary<VehicleType, decimal> _tariffs
        {
            get { return tariffs; }
        }
    }
}