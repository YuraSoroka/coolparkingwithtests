﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        #region старая версия
        /*
        private Timer timer;

        public Timer Timer
        {
            get
            {
                if (timer != null)
                {
                    return timer;
                }
                return timer = new Timer();
            }
        }
        public double Interval { get; set; }//{ get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }


        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            //Timer timer = new Timer();
            //timer.Interval = Interval;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
        */
        #endregion
        public double Interval { get; set ; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public void Start()
        {
        }

        public void Stop()
        {
            //throw new NotImplementedException();
        }
    }
}